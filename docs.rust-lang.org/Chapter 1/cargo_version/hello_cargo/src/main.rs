/*
Cargo expects your source files to live inside the src directory so that the top-level project directory is just for READMEs, 
license information, configuration files, and anything else not related to your code. 
*/

fn main() {
    println!("Hello, world!");
}
