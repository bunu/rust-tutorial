fn main(){
    //println! is a macro.
    //if it wasn't a macro, it would be like this: println()
    //! indicates that it is a macro
    println!("Hello, world!");
}