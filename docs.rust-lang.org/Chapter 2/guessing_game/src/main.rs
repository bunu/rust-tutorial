//using external library. equivalent to "use rand"
//cargo doc --open will open up the documentation
extern crate rand;

//bringing in io library from the standard library
use std::io;
//need to bring in this in order to flush stdout
use std::io::Write;
//brings in cmp::Ordering type used to compare two values
use std::cmp::Ordering;
//using specific trait from rand. trait will be explained later
use rand::Rng;

fn main() {
    println!("Guess the number!");

    //thread_rng() uses a rng that is local to the current thread of execution
    //seeded by computer. gen_range is inclusive on the lower bound but
    //exclusive on the upper bound. Ex: gen_range(1,101) -> 1...100
    let secret_number = rand::thread_rng().gen_range(1,101);
    
    //infinite loop. kinda like while(True)
    loop{
        //changed this to allow same line input
        print!("Please input your guess: ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        //let keyword creates variables
        //variables are immutable (value cannot be changed after creation) by default
        //mut keyword makes the variable mutable
        //String::new() is a function that will return a new instance of a String
        //::new means that new is an associated function of the String type
        //associated function is also known as a static method
        let mut guess = String::new();

        //if std:io wasn't imported, another way to write this would be std::io::stdin
        //read_line takes whatever the user types in and puts it in a string
        //this is why it takes the string as an argument
        //& means that the argument is a reference. it is not a copy but direct access to the variable
        //by defaults, references are immutable which is why the mut keyword is needed
        //shorten version of read_line: it returns a result. it will be ok or err. Err means
        //operation failed. If it is an err, expect will cause the program to crash and display
        //the message written. If it is an ok, expect will take the return value that ok is holding
        //and return that value to you so you can use it. In this case, the value is the number
        //of bytes that the user has entered into the standard input
        io::stdin().read_line(&mut guess).expect("Failed to read line");
        //expect isn't needed but the compiler will throw a warning

        //added this myself to handle quitting the game
        match guess.trim().as_ref(){
            "q" => {
                println!("Quitting game");
                break;
            },

            _ => ()
        }
        //u32 is unsigned 32-bit number
        //i32 is signed 32-bit number
        //guess was created above. this declaration shadows the original variable
        //so we don't have to create different variables to hold different types
        //.trim() will remove any whitespaces in the beginning and end and \n
        //parse() will convert the input. it looks at what is after: for the type
        //which is u32 in this case. parse can fail so expect is used

        //switching from expect to match is how you generally move from crash on error
        //to error handling
        let guess: u32 = match guess.trim().parse(){
            //if string can be converted to u32, parse() will return Ok value that contains the resulting number
            //it will return the num value that parse() produced
            Ok(num) => num,
            //if parse() cannont convert,  it will return an Err value that contains more info about the error
            // _ is a catcahll value so this will match all Err values. Continue will work like other languages
            Err(_) => {
                        println!("Please enter a number");
                        continue;
            }
        };


        println!("You guessed: {}",guess);

        //cmp compares two values. cmp returns a variant of the Ordering enum
        //use match expression to decide what to do based on what is returned
        match guess.cmp(&secret_number){
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                                println!("You win!");
                                //acts like break in any other language
                                break;
            }
        }
    }
}
